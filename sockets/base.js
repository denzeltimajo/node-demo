module.exports = function (io) {
  io.on('connection', (socket) => {
    console.log("ID " + socket.id + " has connected");

    socket.on('new message', function (data) {
      console.log("\x1b[46m%s\x1b[0m %s", socket.id , data);
      // we tell the client to execute 'new message'
      socket.broadcast.emit('new message', {
        username: socket.id,
        message: data
      });
    });

    socket.on('disconnect', (reason) => {
      console.log('\x1b[41m%s\x1b[0m%s',"Disconneted", " ID " + socket.id + " : ("+ reason +")");
    });
  });
}
