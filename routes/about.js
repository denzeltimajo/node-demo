var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');

var url = 'mongodb://localhost:27017/test';
var name = 'about';

/* GET about page. */
router.get('/', function(req, res, next) {
  res.render('about', { title: 'About' , name: name,
    styles:[ "bootstrap.min" , "crud-style"] ,
    javascripts:["jquery-3.2.1.min","bootstrap.min"],
    items: []
  });
});

router.get('/get-data', function(req, res, next){
  var resultArray = [];
  mongo.connect(url, function(err, db){
    assert.equal(null,err);
    var cursor = db.collection('user-data').find();
    cursor.forEach(function(doc, err){
      assert.equal(null,err);
      resultArray.push(doc);
    }, function(){
      db.close();
      res.render('about', { title: 'About' , name: name,
        styles:[ "bootstrap.min" , "crud-style"] ,
        javascripts:["jquery-3.2.1.min","bootstrap.min"],
        items: resultArray});
    });
  })
});

router.post('/insert', function(req, res, next){
  var item = {
    title: req.body.title,
    content: req.body.content,
    author: req.body.author,
  };

  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    db.collection('user-data').insertOne(item, function(erro, result){
      assert.equal(null,erro);
      console.log('Inserted: ' + item);
      db.close();
    });
  })

  res.redirect('/'+name+'/');
});

router.post('/update', function(req, res, next){
  var item = {
    title: req.body['title-update'],
    content: req.body['content-update'],
    author: req.body['author-update'],
  };

  var id = req.body['id-update'];

  console.log(req.body);

  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    db.collection('user-data').updateOne({"_id": objectId(id)}, { $set: item } , function(erro, result){
      assert.equal(null,erro);
      console.log('Updated: ' + item);
      db.close();
    });
  })

  res.redirect('/'+name+'/');

});

router.post('/delete', function(req, res, next){
  var id = req.body['id-delete'];

  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    db.collection('user-data').deleteOne({"_id": objectId(id)} , function(erro, result){
      assert.equal(null,erro);
      console.log('Deleted: ' + id);
      db.close();
    });
  })

  res.redirect('/'+name+'/');
});


module.exports = router;
